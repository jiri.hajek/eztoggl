# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## [0.4.0] - 2025-01-18
### Changed
- migrate Fakturoid integration to API v3

## [0.3.4] - 2024-04-26
### Fixed
- Toggl detailed report pdf

## [0.3.3] - 2024-04-26
### Changed
- migrate Toggl v8 API calls to Toggl v9 API

## [0.3.2] - 2022-07-01
### Added
- `--upload-invoice` and `--upload-reports` options

## [0.3.1] - 2022-06-24
### Fixed
- make `fakturoid.invoice.unit_name` in `config.json` optional again

## [0.3.0] - 2022-06-23
### Added
- `default_project_name` option to `config.json`

### Changed
- use `default_hourly_rate` when project's `hourly_rate` is missing

### Fixed
- **Breaking change:** fix validation of `config.json` --- `fakturoid.invoice.unit_name` is now required

## [0.2.3] - 2022-04-01
### Added
- `-v` / `--version` flag to print the current version

### Fixed
- Fix lookup of Google Drive folders from shared drives

## [0.2.2] - 2021-05-21
### Changed
- Make `log`, `oauth2util` and `timeutil` packages private

### Fixed
- Merge invoice lines with the same name and unit price into a single line (#1)

## [0.2.1] - 2021-04-28
### Changed
- Improve `config.example.json` to prevent confusion

### Fixed
- Fix Makefile-related issues
- Fix invalid `go install` command in README

## [0.2.0] - 2021-04-07
### Changed
- Improve lookup of `config.json`
- Use platform-independent path joins

## [0.1.0] - 2021-04-04
### Added
- Initial release
