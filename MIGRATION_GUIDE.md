# Migration guide
## Updating from v0.3.4 to v0.4.0
This update contains migration from the deprecated Fakturoid API v2 to the new Fakturoid API v3, which no longer supports API token authentication. To continue using this integration, replace your API token with your OAuth2 client ID and client secret. You can find those in *Settings* > *User account* after logging in to your Fakturoid account.

Replace `fakturoid.api_token` with `fakturoid.client_id` and `fakturoid.client_secret` in your `config.json`. See [config.example.json](config.example.json) for an example.
