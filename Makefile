VERSION = v0.4.0

MAIN_GO_PATH = cmd/eztoggl/main.go

EXECUTABLE_PREFIX = eztoggl-
LINUX_AMD64_EXECUTABLE = $(EXECUTABLE_PREFIX)$(VERSION)-linux-amd64
WINDOWS_AMD64_EXECUTABLE = $(EXECUTABLE_PREFIX)$(VERSION)-windows-amd64
MACOS_AMD64_EXECUTABLE = $(EXECUTABLE_PREFIX)$(VERSION)-macos-amd64
MACOS_ARM64_EXECUTABLE = $(EXECUTABLE_PREFIX)$(VERSION)-macos-arm64

ATTACHMENTS = credentials.json config.example.json

.PHONY: build build_release package clean

all: build

build:
	go build -ldflags="-X 'gitlab.com/jiri.hajek/eztoggl/internal/build.Version=development'" -o dist/eztoggl $(MAIN_GO_PATH)

build_release: clean
	CGO_ENABLED=0 GOOS=linux   GOARCH=amd64 go build -ldflags="-s -w -X 'gitlab.com/jiri.hajek/eztoggl/internal/build.Version=$(VERSION)'" -o $(LINUX_AMD64_EXECUTABLE) $(MAIN_GO_PATH)
	CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -ldflags="-s -w -X 'gitlab.com/jiri.hajek/eztoggl/internal/build.Version=$(VERSION)'" -o $(WINDOWS_AMD64_EXECUTABLE).exe $(MAIN_GO_PATH)
	CGO_ENABLED=0 GOOS=darwin  GOARCH=amd64 go build -ldflags="-s -w -X 'gitlab.com/jiri.hajek/eztoggl/internal/build.Version=$(VERSION)'" -o $(MACOS_AMD64_EXECUTABLE)  $(MAIN_GO_PATH)
	CGO_ENABLED=0 GOOS=darwin  GOARCH=arm64 go build -ldflags="-s -w -X 'gitlab.com/jiri.hajek/eztoggl/internal/build.Version=$(VERSION)'" -o $(MACOS_ARM64_EXECUTABLE) $(MAIN_GO_PATH)

package: build_release
	mkdir dist
	# Package Linux release
	sha256sum $(LINUX_AMD64_EXECUTABLE) > checksum.sha256
	tar --gzip -cf dist/$(LINUX_AMD64_EXECUTABLE).tar.gz $(LINUX_AMD64_EXECUTABLE) checksum.sha256 $(ATTACHMENTS)
	zip -q dist/$(LINUX_AMD64_EXECUTABLE).zip $(LINUX_AMD64_EXECUTABLE) checksum.sha256 $(ATTACHMENTS)
	# Package Windows release
	sha256sum $(WINDOWS_AMD64_EXECUTABLE).exe > checksum.sha256
	tar --gzip -cf dist/$(WINDOWS_AMD64_EXECUTABLE).tar.gz $(WINDOWS_AMD64_EXECUTABLE).exe checksum.sha256 $(ATTACHMENTS)
	zip -q dist/$(WINDOWS_AMD64_EXECUTABLE).zip $(WINDOWS_AMD64_EXECUTABLE).exe checksum.sha256 $(ATTACHMENTS)
	# Package macOS (Intel) release
	sha256sum $(MACOS_AMD64_EXECUTABLE) > checksum.sha256
	tar --gzip -cf dist/$(MACOS_AMD64_EXECUTABLE).tar.gz $(MACOS_AMD64_EXECUTABLE) checksum.sha256 $(ATTACHMENTS)
	zip -q dist/$(MACOS_AMD64_EXECUTABLE).zip $(MACOS_AMD64_EXECUTABLE) checksum.sha256 $(ATTACHMENTS)
	# Package macOS (M1) release
	sha256sum $(MACOS_ARM64_EXECUTABLE) > checksum.sha256
	tar --gzip -cf dist/$(MACOS_ARM64_EXECUTABLE).tar.gz $(MACOS_ARM64_EXECUTABLE) checksum.sha256 $(ATTACHMENTS)
	zip -q dist/$(MACOS_ARM64_EXECUTABLE).zip $(MACOS_ARM64_EXECUTABLE) checksum.sha256 $(ATTACHMENTS)
	# Remove leftover files
	rm -f $(LINUX_AMD64_EXECUTABLE) $(WINDOWS_AMD64_EXECUTABLE).exe $(MACOS_AMD64_EXECUTABLE) $(MACOS_ARM64_EXECUTABLE) checksum.sha256

clean:
	rm -rf dist
