# eztoggl
[![Go Reference](https://pkg.go.dev/badge/gitlab.com/jiri.hajek/eztoggl.svg)](https://pkg.go.dev/gitlab.com/jiri.hajek/eztoggl)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/jiri.hajek/eztoggl)](https://goreportcard.com/report/gitlab.com/jiri.hajek/eztoggl)

## Table of contents
* [About](#about)
* [Installation](#installation)
* [Usage](#usage)
* [Configuration](#configuration)
* [Public packages](#public-packages)
* [Google Drive permissions](#google-drive-permissions)

## About
*eztoggl* is a command-line tool that aims to simplify your monthly Toggl workflow. It's basically a refactor of [toggl2gdrive](https://gitlab.com/jiri.hajek/toggl2gdrive) with a couple of extra features.

With the press of a button, *eztoggl* can:
* generate both summary and detailed [Toggl Track](https://toggl.com/track) PDF reports for last month
* generate a PDF invoice using [Fakturoid](https://fakturoid.cz)
* save generated files to local storage
* upload generated files to Google Drive

**NOTE:** Google Drive integration only works with accounts in the `*@applifting.cz` domain because the Google app whose credentials are specified in `credentials.json` is limited to internal use. If you'd like to use it for other accounts, you'll have to create your own Google Cloud Platform project, then create *OAuth client ID* credentials [here](https://console.cloud.google.com/apis/credentials), download them and replace the existing `credentials.json`. When prompted for *Application type*, select *Web application* and add `http://localhost:3000` to the list of *Authorized redirect URIs*. I might go through the app verification process with Google in the future to make Google Drive integration available to everyone out-of-the-box.

## Installation
There are 3 main options to install *eztoggl*.

### Using `go install`
```
go install gitlab.com/jiri.hajek/eztoggl/cmd/eztoggl@latest
```

### Clone and build
Clone the repository and build the project yourself. The executable will be saved to a directory called `dist`.

```
git clone https://gitlab.com/jiri.hajek/eztoggl.git
cd eztoggl
make
```

### Binary release
If you don't want to build the app yourself, you can grab the latest binary from [releases](https://gitlab.com/jiri.hajek/eztoggl/-/releases).

## Usage
![demo](images/demo.gif)

```
Usage: eztoggl [-hiIrRsuv] [-o DIRECTORY] [parameters ...]
 -h, --help     show help
 -i, --invoice  generate a Fakturoid invoice
 -I, --upload-invoice
                upload invoice to Google Drive
 -o DIRECTORY   local output directory for generated files [.]
 -r, --reports  generate Toggl reports
 -R, --upload-reports
                upload Toggl reports to Google Drive
 -s, --save     save generated files
 -u, --upload   upload all generated files to Google Drive
 -v, --version  print version
```

For even more convenience, you can set up *eztoggl* to automatically run each month with something like cron or anacron.
```
# crontab
# Upload Toggl reports and Fakturoid invoice to Google Drive on the 1st day of each month at 6 AM
0 6 1 * * /path/to/eztoggl -iru
```

If you decide to upload your files to Google Drive, you will be prompted for authorization the first time run *eztoggl* with the `-u` / `--upload` flag. The app will automatically open a browser tab with the Google authorization flow for you. Once you give *eztoggl* the necessary permissions, you will be redirect to a local web server running in the background, which will automatically collect the authorization code for you. The generated OAuth2 tokens will be saved to `token.json` inside your config directory.

As long as `token.json` exists, *eztoggl* will not prompt you for authorization and will instead refresh the existing token as needed. Keep in mind that the app only receives your refresh token on the first authorization. Therefore, if you lose your `token.json`, you'll have to:
1. remove the app's access from your Google account's security settings
2. go through the Google authorization flow again

If you skip step 1, your tokens will only lasts for 1 hour and *eztoggl* won't be able to refresh them.

## Configuration
Create a `config.json` file based on the configuration example in [`config.example.json`](config.example.json).

`config.json` is separated into 3 main sections:
1. `fakturoid`
2. `gdrive`
3. `toggl`

You can remove the sections you don't intend to use, e.g. if you don't want to upload files to Google Drive, you can remove the `gdrive` section.

### Config directory
*eztoggl* looks for `config.json` in the following directories, in this specific order:
1. current directory
2. `(config directory)/eztoggl` as per [`os.UserConfigDir()`](https://golang.org/pkg/os/#UserConfigDir)
* Linux: `$XDG_CONFIG_HOME/eztoggl` if  `$XDG_CONFIG_HOME` is defined, else `$HOME/.config/eztoggl`
* macOS: `$HOME/Library/Application Support/eztoggl`
* Windows: `%AppData%\eztoggl`
3. `(home directory)/.eztoggl` as per [`os.UserHomeDir()`](https://golang.org/pkg/os/#UserHomeDir)
* Linux: `$HOME/.eztoggl`
* macOS: `$HOME/.eztoggl`
* Windows: `%USERPROFILE%\.eztoggl`

The first encountered directory that contains `config.json` will also be used for `credentials.json` and `token.json`.

### Fakturoid slug
To find out what your Fakturoid slug is, go to https://app.fakturoid.cz and log in. You will then be redirected to the Fakturoid dashboard and your slug will be in the URL - `https://app.fakturoid.cz/yourslugishere/dashboard`.

For example, in my case the URL is `https://app.fakturoid.cz/jirihajek1/dashboard` so my slug is `jirihajek1`.

### Fakturoid API credentials
This application uses the OAuth2 Client Credentials flow to authenticate with the Fakturoid API. To obtain your `client_id` and `client_secret`, log in to Fakturoid and go to *Settings* > *User account*.

### Structure of `config.json`
| Key | Type | Description |
|:----|:-----|:------------|
| fakturoid | object | Fakturoid configuration, can be omitted if you don't intend to generate an invoice |
| fakturoid.client\_id | string | Fakturoid API client ID |
| fakturoid.client\_secret | string | Fakturoid API client secret |
| fakturoid.email | string | Fakturoid email |
| fakturoid.slug | string | Fakturoid slug, see [Fakturoid slug](#fakturoid-slug) for more information |
| fakturoid.invoice | object | Fakturoid invoice configuration |
| fakturoid.invoice.prefix\_year\_and\_month | bool | Prefix invoice with year and month (e.g. `file.pdf` becomes `YY_MM_file.pdf`) |
| fakturoid.invoice.filename | string | Invoice filename (including the `.pdf` extension) |
| fakturoid.invoice.bank\_account\_name | string | Name of the Fakturoid bank account to use |
| fakturoid.invoice.client\_name | string | Name of the Fakturoid client to invoice |
| fakturoid.invoice.currency | string | Currency to be used in the invoice (3letter code) |
| fakturoid.invoice.days\_until\_due | int | Amount of days until the due date |
| fakturoid.invoice.default\_hourly\_rate | int | Default hourly rate |
| fakturoid.invoice.default\_project\_name | string | Default project name used in the invoice |
| fakturoid.invoice.unit\_name | string | Unit name to be used for invoice items, e.g. `h`, `hours` or an empty string |
| fakturoid.invoice.vat\_rate | int | VAT rate, usually either 0 or 21 |
| fakturoid.invoice.projects | object | Object that maps Toggl project names to names and hourly rates that should be used in the invoice |
| fakturoid.invoice.projects[KEY].name | string | Name that should be used in the invoice for the given project |
| fakturoid.invoice.projects[KEY].hourly\_rate | int | Hourly rate that should be used in the invoice for the given project |
| gdrive | object | Google Drive configuration, can be omitted if you don't intend to upload files to Google Drive |
| gdrive.reports\_folder\_name | string | Name of the Google Drive folder where your Toggl reports should be uploaded |
| gdrive.invoice\_folder\_name | string | Name of the Google Drive folder where your invoice should be uploaded |
| toggl | object | Toggl configuration, cannot be omitted because the Toggl API will always be used |
| toggl.api\_token | string | Toggl API token |
| toggl.workspace\_name | string | Name of the Toggl workspace to generate reports for |
| toggl.client\_name | string | Name of the Toggl client to generate reports for |
| toggl.reports | object | Toggl reports configuration |
| toggl.reports.prefix\_year\_and\_month | bool | Prefix reports with year and month (e.g. `file.pdf` becomes `YY_MM_file.pdf`) |
| toggl.reports.detailed\_report\_filename | string | Detailed report filename (including the `.pdf` extension) |
| toggl.reports.detailed\_report\_filename | string | Summary report filename (including the `.pdf` extension) |

### Invoice item names and hourly rates
If there is a project in your Toggl summary that is not present in `config.json`, *eztoggl* will fall back to invoice item name from:
* `fakturoid.invoice.default_project_name` if configured
* the project's name from Toggl if the default is not configured

If you don't set a project's hourly rate, *eztoggl* will fall back to `fakturoid.invoice.default_hourly_rate`.

## Public packages
I tried to separate the main logic into packages so that others could reuse it in their projects. You might find these useful if you're implementing something similar.
* [gdriveutil](https://pkg.go.dev/gitlab.com/jiri.hajek/eztoggl/pkg/gdriveutil) - Google Drive integration
* [fakturoid](https://pkg.go.dev/gitlab.com/jiri.hajek/eztoggl/pkg/fakturoid) - Fakturoid integration
* [toggl](https://pkg.go.dev/gitlab.com/jiri.hajek/eztoggl/pkg/toggl) - Toggl Track integration

## Google Drive permissions
If you decide to use the built-in Google Drive integration, *eztoggl* will ask you for 2 scopes of the Google Drive API v3, specifically `drive.metadata.readonly` and `drive.file`. All scopes along with a short description can be found [here](https://developers.google.com/identity/protocols/oauth2/scopes#drive).

`drive.metadata.readonly` grants the app read-only access to metadata of files in your Google Drive. It is needed to find the folders where your files should be uploaded.

`drive.file` allows the app to view and manage Google Drive files and folders that have been opened or created by the app. It is needed to upload your files.
