package main

import (
	"bytes"
	"fmt"
	"math"
	"os"
	"path/filepath"
	"strconv"

	"github.com/google/uuid"
	"google.golang.org/api/drive/v3"

	"gitlab.com/jiri.hajek/eztoggl/pkg/fakturoid"
	"gitlab.com/jiri.hajek/eztoggl/pkg/gdriveutil"
	"gitlab.com/jiri.hajek/eztoggl/pkg/toggl"

	"gitlab.com/jiri.hajek/eztoggl/internal/env"
	"gitlab.com/jiri.hajek/eztoggl/internal/log"
	"gitlab.com/jiri.hajek/eztoggl/internal/timeutil"
)

type lineID struct {
	name string
	rate int
}

func main() {
	err := env.Initialize()
	if err != nil {
		log.Error.Fatalf("could not initialize environment: %s", err)
	}

	if env.Config.Toggl == nil {
		log.Error.Fatalln("toggl configuration was not found in your config.json")
	}

	tgl := toggl.New(
		env.Config.Toggl.APIToken,
	)

	togglWorkspaceID, err := tgl.GetWorkspaceID(env.Config.Toggl.WorkspaceName)
	if err != nil {
		log.Error.Fatalf("could not get toggl workspace: %s", err)
	}

	togglClientID, err := tgl.GetClientID(togglWorkspaceID, env.Config.Toggl.ClientName)
	if err != nil {
		log.Error.Fatalf("could not get toggl client: %s", err)
	}

	var fkt *fakturoid.Fakturoid
	var fakturoidBankAccountID int
	var fakturoidClientID int
	if env.Flag.Invoice {
		if env.Config.Fakturoid == nil {
			log.Error.Fatalln("fakturoid configuration was not found in your config.json")
		}

		fkt = fakturoid.New(
			env.Config.Fakturoid.ClientID,
			env.Config.Fakturoid.ClientSecret,
			env.Config.Fakturoid.Email,
			env.Config.Fakturoid.Slug,
		)

		fakturoidBankAccountID, err = fkt.GetBankAccountID(env.Config.Fakturoid.Invoice.BankAccountName)
		if err != nil {
			log.Error.Fatalf("could not get fakturoid bank account: %s", err)
		}

		fakturoidClientID, err = fkt.GetClientID(env.Config.Fakturoid.Invoice.ClientName)
		if err != nil {
			log.Error.Fatalf("could not get fakturoid client: %s", err)
		}
	}

	var gdriveService *drive.Service
	var reportsFolderID string
	var invoiceFolderID string
	if env.Flag.Upload || env.Flag.UploadInvoice || env.Flag.UploadReports {
		if env.Config.GDrive == nil {
			log.Error.Fatalln("gdrive configuration was not found in your config.json")
		}

		pathToCredentials := filepath.Join(env.BaseConfigPath, "credentials.json")
		pathToToken := filepath.Join(env.BaseConfigPath, "token.json")
		gdriveService, err = gdriveutil.GetService(
			pathToCredentials,
			pathToToken,
			drive.DriveMetadataReadonlyScope,
			drive.DriveFileScope,
		)
		if err != nil {
			log.Error.Fatalf("could not get gdrive service: %s", err)
		}

		if env.Flag.Reports {
			reportsFolderID, err = gdriveutil.GetFolderID(gdriveService, env.Config.GDrive.ReportsFolderName)
			if err != nil {
				log.Error.Fatalf("could not get reports folder ID: %s", err)
			}
		}

		if env.Flag.Invoice {
			invoiceFolderID, err = gdriveutil.GetFolderID(gdriveService, env.Config.GDrive.ReportsFolderName)
			if err != nil {
				log.Error.Fatalf("could not get invoice folder ID: %s", err)
			}
		}
	}

	if env.Flag.Save {
		uuidString := uuid.New().String()
		pathToTestFile := filepath.Join(env.OutputDirectory, uuidString)
		err = os.WriteFile(pathToTestFile, []byte{}, 0644)
		if err != nil {
			log.Error.Fatalf("cannot write to output directory: %s", err)
		}
		os.Remove(pathToTestFile)
	}

	from, to := timeutil.GetLastMonthsBoundaries()
	targetYear, targetMonth, _ := from.Date()
	targetYearAbbrev := targetYear % 100
	prefix := fmt.Sprintf("%d_%02d_", targetYearAbbrev, targetMonth)

	if env.Flag.Reports {
		log.Info.Println()

		log.Info.Println("Generating summary report ...")
		summaryReportData, err := tgl.GetSummaryReportPDF(togglWorkspaceID, togglClientID, from, to)
		if err != nil {
			log.Error.Fatalln(err)
		}

		log.Info.Println("Generating detailed report ...")
		detailedReportData, err := tgl.GetDetailedReportPDF(togglWorkspaceID, togglClientID, from, to)
		if err != nil {
			log.Error.Fatalln(err)
		}

		log.Info.Println("Reports generated successfully!")

		summaryReportName := env.Config.Toggl.Reports.SummaryReportFilename
		detailedReportName := env.Config.Toggl.Reports.DetailedReportFilename

		if env.Config.Toggl.Reports.PrefixYearAndMonth {
			summaryReportName = fmt.Sprintf("%s%s", prefix, summaryReportName)
			detailedReportName = fmt.Sprintf("%s%s", prefix, detailedReportName)
		}

		if env.Flag.Save {
			log.Info.Println()
			log.Info.Println("Saving reports ...")

			pathToSummaryReport := filepath.Join(env.OutputDirectory, summaryReportName)
			err = os.WriteFile(pathToSummaryReport, summaryReportData, 0644)
			if err != nil {
				log.Error.Fatalln(err)
			}
			log.Info.Printf("Summary report saved to %s", pathToSummaryReport)

			pathToDetailedReport := filepath.Join(env.OutputDirectory, detailedReportName)
			err = os.WriteFile(pathToDetailedReport, detailedReportData, 0644)
			if err != nil {
				log.Info.Println("Could not save detailed report.")
			}
			log.Info.Printf("Detailed report saved to %s", pathToDetailedReport)

			log.Info.Println("Reports saved successfully!")
		}

		if env.Flag.Upload || env.Flag.UploadReports {
			log.Info.Println()

			log.Info.Println("Uploading summary report ...")
			_, err = gdriveutil.UploadFileToFolder(
				gdriveService,
				reportsFolderID,
				summaryReportName,
				"application/pdf",
				bytes.NewReader(summaryReportData),
			)
			if err != nil {
				log.Error.Fatalln(err)
			}

			log.Info.Println("Uploading detailed report ...")
			_, err = gdriveutil.UploadFileToFolder(
				gdriveService,
				reportsFolderID,
				detailedReportName,
				"application/pdf",
				bytes.NewReader(detailedReportData),
			)
			if err != nil {
				log.Error.Fatalln(err)
			}

			log.Info.Println("Reports uploaded successfully!")
		}
	}

	if env.Flag.Invoice {
		log.Info.Println()

		log.Info.Println("Getting summary data ...")
		summary, err := tgl.GetSummary(togglWorkspaceID, togglClientID, from, to)
		if err != nil {
			log.Error.Fatalln(err)
		}

		log.Info.Println("Generating invoice ...")

		year, month, day := to.Date()
		dateIssued := fmt.Sprintf("%d-%02d-%02d", year, month, day)

		lineMap := make(map[lineID]fakturoid.InvoiceLine)
		lineQuantityMap := make(map[lineID]float64)
		for _, entry := range summary.Entries {
			var name string
			var hourlyRate int
			project, ok := env.Config.Fakturoid.Invoice.Projects[entry.Project]

			if !ok {
				if env.Config.Fakturoid.Invoice.DefaultProjectName != "" {
					name = env.Config.Fakturoid.Invoice.DefaultProjectName
				} else {
					log.Warn.Printf(
						"missing item name and hourly rate for project %q, using defaults (%s, %d %s/h)",
						entry.Project,
						entry.Project,
						env.Config.Fakturoid.Invoice.DefaultHourlyRate,
						env.Config.Fakturoid.Invoice.Currency,
					)

					name = entry.Project
				}
			} else {
				name = project.Name
			}

			if project.HourlyRate == 0 {
				hourlyRate = env.Config.Fakturoid.Invoice.DefaultHourlyRate
			} else {
				hourlyRate = project.HourlyRate
			}

			key := lineID{
				name: name,
				rate: hourlyRate,
			}

			_, ok = lineMap[key]
			if !ok {
				lineMap[key] = fakturoid.InvoiceLine{
					Name:      name,
					UnitName:  env.Config.Fakturoid.Invoice.UnitName,
					UnitPrice: strconv.Itoa(hourlyRate),
					VatRate:   env.Config.Fakturoid.Invoice.VatRate,
				}
			}

			lineQuantityMap[key] += entry.Time
		}

		invoiceLines := []fakturoid.InvoiceLine{}
		for key, line := range lineMap {
			quantity := lineQuantityMap[key]
			roundedQuantity := math.Round(quantity*100) / 100
			line.Quantity = fmt.Sprintf("%.2f", roundedQuantity)
			invoiceLines = append(invoiceLines, line)
		}

		invoice := fakturoid.NewInvoice(
			fakturoidBankAccountID,
			fakturoidClientID,
			env.Config.Fakturoid.Invoice.Currency,
			dateIssued,
			env.Config.Fakturoid.Invoice.DaysUntilDue,
			invoiceLines,
		)

		invoicePDF, err := fkt.GenerateInvoice(invoice)
		if err != nil {
			log.Error.Fatalf("could not generate invoice: %s", err)
		}
		log.Info.Println("Invoice generated successfully!")

		invoiceName := env.Config.Fakturoid.Invoice.Filename
		if env.Config.Fakturoid.Invoice.PrefixYearAndMonth {
			invoiceName = fmt.Sprintf("%s%s", prefix, invoiceName)
		}

		if env.Flag.Save {
			log.Info.Println()
			log.Info.Println("Saving invoice ...")

			pathToInvoice := filepath.Join(env.OutputDirectory, invoiceName)
			err = os.WriteFile(pathToInvoice, invoicePDF, 0644)
			if err != nil {
				log.Error.Fatalln(err)
			}
			log.Info.Printf("Invoice saved to %s", pathToInvoice)
			log.Info.Println("Invoice saved successfully!")
		}

		if env.Flag.Upload || env.Flag.UploadInvoice {
			log.Info.Println()

			log.Info.Println("Uploading invoice ...")
			_, err = gdriveutil.UploadFileToFolder(
				gdriveService,
				invoiceFolderID,
				invoiceName,
				"application/pdf",
				bytes.NewReader(invoicePDF),
			)
			if err != nil {
				log.Error.Fatalln(err)
			}

			log.Info.Println("Invoice uploaded successfully!")
		}
	}
}
