package env

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/go-playground/validator"
	"github.com/pborman/getopt/v2"

	"gitlab.com/jiri.hajek/eztoggl/internal/build"
	"gitlab.com/jiri.hajek/eztoggl/internal/log"
)

type flags struct {
	Help          bool
	Version       bool
	Invoice       bool
	Reports       bool
	Save          bool
	Upload        bool
	UploadReports bool
	UploadInvoice bool
}

// OutputDirectory stores the output directory for generated files.
var OutputDirectory string

type fakturoidConfig struct {
	ClientID     string                 `json:"client_id" validate:"required"`
	ClientSecret string                 `json:"client_secret" validate:"required"`
	Email        string                 `json:"email" validate:"required,email"`
	Slug         string                 `json:"slug" validate:"required"`
	Invoice      fakturoidInvoiceConfig `json:"invoice" validate:"required"`
}

type projectMetadata struct {
	Name       string `json:"name" validate:"required"`
	HourlyRate int    `json:"hourly_rate" validate:"gte=0"`
}

type fakturoidInvoiceConfig struct {
	BankAccountName    string `json:"bank_account_name" validate:"required"`
	ClientName         string `json:"client_name" validate:"required"`
	Currency           string `json:"currency" validate:"required,len=3"`
	DaysUntilDue       int    `json:"days_until_due" validate:"gte=0"`
	DefaultHourlyRate  int    `json:"default_hourly_rate" validate:"gte=0"`
	DefaultProjectName string `json:"default_project_name"`
	Filename           string `json:"filename" validate:"required"`
	PrefixYearAndMonth bool   `json:"prefix_year_and_month"`
	UnitName           string `json:"unit_name"`
	VatRate            int    `json:"vat_rate" validate:"gte=0"`

	Projects map[string]projectMetadata `json:"projects"`
}

type gdriveConfig struct {
	InvoiceFolderName string `json:"invoice_folder_name" validate:"required"`
	ReportsFolderName string `json:"reports_folder_name" validate:"required"`
}

type togglReportsConfig struct {
	PrefixYearAndMonth     bool   `json:"prefix_year_and_month"`
	DetailedReportFilename string `json:"detailed_report_filename" validate:"required"`
	SummaryReportFilename  string `json:"summary_report_filename" validate:"required"`
}

type togglConfig struct {
	APIToken      string             `json:"api_token" validate:"required"`
	WorkspaceName string             `json:"workspace_name" validate:"required"`
	ClientName    string             `json:"client_name" validate:"required"`
	Reports       togglReportsConfig `json:"reports" validate:"required"`
}

// ConfigJSON struct corresponds to the schmema of the `config` file.
type configJSON struct {
	Fakturoid *fakturoidConfig `json:"fakturoid"`
	GDrive    *gdriveConfig    `json:"gdrive"`
	Toggl     *togglConfig     `json:"toggl"`
}

// UserAgent is the value used in the User-Agent header for API calls
const UserAgent = "gitlab.com/jiri.hajek/eztoggl (jiri@jirihajek.xyz)"

// Flag represents a set of CLI flags set by the user.
var Flag flags

// Config stores configuration from `config.json`.
var Config configJSON

// BaseConfigPath stores path to directory with `config.json` that is being used.
var BaseConfigPath string

func parseValidationError(err error) error {
	if _, ok := err.(*validator.InvalidValidationError); ok {
		return err
	}

	var errorList string
	for _, err := range err.(validator.ValidationErrors) {
		var param string
		if len(err.Param()) > 0 {
			param = fmt.Sprintf(" %s", err.Param())
		}

		errorList = fmt.Sprintf("%s%s constraint: %s%s, got %v\n", errorList, err.StructNamespace(), err.ActualTag(), param, err.Value())
	}
	errorList = strings.TrimSuffix(errorList, "\n")

	return fmt.Errorf("found errors in config.json:\n%s", errorList)
}

// Initialize initializes `Flag` and `Config` with parsed CLI flags and `config.json`.
func Initialize() error {
	getopt.FlagLong(&Flag.Help, "help", 'h', "show help")
	getopt.FlagLong(&Flag.Version, "version", 'v', "print version")
	getopt.FlagLong(&Flag.Invoice, "invoice", 'i', "generate a Fakturoid invoice")
	getopt.FlagLong(&Flag.Reports, "reports", 'r', "generate Toggl reports")
	getopt.FlagLong(&Flag.Save, "save", 's', "save generated files")
	getopt.FlagLong(&Flag.Upload, "upload", 'u', "upload all generated files to Google Drive")
	getopt.FlagLong(&Flag.UploadInvoice, "upload-invoice", 'I', "upload invoice to Google Drive")
	getopt.FlagLong(&Flag.UploadReports, "upload-reports", 'R', "upload Toggl reports to Google Drive")
	output := getopt.String('o', ".", "local output directory for generated files", "DIRECTORY")
	getopt.Parse()

	OutputDirectory = *output

	if Flag.Help {
		getopt.Usage()
		os.Exit(0)
	}

	if Flag.Version {
		fmt.Println(build.Version)
		os.Exit(0)
	}

	if !((Flag.Invoice && (Flag.Save || Flag.Upload || Flag.UploadInvoice)) || (Flag.Reports && (Flag.Save || Flag.Upload || Flag.UploadReports))) {
		log.Info.Println("Nothing to do")
		getopt.Usage()
		os.Exit(0)
	}

	allConfigPaths := []string{}

	currentDir, err := os.Getwd()
	if err == nil {
		allConfigPaths = append(allConfigPaths, currentDir)
	}

	configDir, err := os.UserConfigDir()
	if err == nil {
		allConfigPaths = append(allConfigPaths, filepath.Join(configDir, "eztoggl"))
	}

	homeDir, err := os.UserHomeDir()
	if err == nil {
		allConfigPaths = append(allConfigPaths, filepath.Join(homeDir, ".eztoggl"))
	}

	if len(allConfigPaths) == 0 {
		return err
	}

	found := false
	for _, path := range allConfigPaths {
		pathToConfigJSON := filepath.Join(path, "config.json")
		bytes, err := os.ReadFile(pathToConfigJSON)
		if err != nil {
			continue
		}

		err = json.Unmarshal(bytes, &Config)
		if err != nil {
			return fmt.Errorf("could not parse configuration from %s (%s)", pathToConfigJSON, err)
		}

		log.Info.Printf("Using configuration from %s", pathToConfigJSON)
		found = true
		BaseConfigPath = path
		break
	}

	if !found {
		return fmt.Errorf("could not find config.json in any of the available directories")
	}

	validate := validator.New()
	err = validate.Struct(Config)
	if err != nil {
		return parseValidationError(err)
	}

	for _, projectMetadata := range Config.Fakturoid.Invoice.Projects {
		validate := validator.New()
		err = validate.Struct(projectMetadata)
		if err != nil {
			return parseValidationError(err)
		}
	}

	return nil
}
