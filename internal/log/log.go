package log

import (
	"log"
	"os"
)

var (
	// Info logs information to stdout.
	Info *log.Logger = log.New(os.Stdout, "", 0)

	// Warn logs warnings to stderr
	Warn *log.Logger = log.New(os.Stderr, "Warning: ", 0)

	// Error logs errors to stderr
	Error *log.Logger = log.New(os.Stderr, "Error: ", 0)
)
