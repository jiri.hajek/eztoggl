package oauth2util

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"time"

	"golang.org/x/oauth2"

	"gitlab.com/jiri.hajek/eztoggl/internal/log"
)

func openURL(url string) error {
	var cmd string
	var args []string

	switch runtime.GOOS {
	case "windows":
		cmd = "cmd"
		args = []string{"/c", "start"}
	case "darwin":
		cmd = "open"
	default:
		cmd = "xdg-open"
	}
	args = append(args, url)
	return exec.Command(cmd, args...).Start()
}

// GetClient opens a new browser tab with OAuth2 authorization flow, seamlessly retrieves a token by redirecting to a local web server, saves the token to `pathToTokenJSON` and returns the generated client. If the token already exists, it will be reused.
func GetClient(pathToTokenJSON string, config *oauth2.Config) (*http.Client, error) {
	token, err := tokenFromFile(pathToTokenJSON)
	if err != nil {
		token, err = getTokenFromWeb(config)
		if err != nil {
			return nil, fmt.Errorf("could not retrieve token from the web")
		}

		saveToken(pathToTokenJSON, token)
	}

	return config.Client(context.Background(), token), nil
}

func getTokenFromWeb(config *oauth2.Config) (*oauth2.Token, error) {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)

	server := http.Server{
		Addr:    ":3000",
		Handler: nil,
	}

	tokenChannel := make(chan string)
	defer close(tokenChannel)

	redirectHandler := func(w http.ResponseWriter, r *http.Request) {
		authCode := r.URL.Query().Get("code")

		if authCode != "" {
			tokenChannel <- authCode
			io.WriteString(
				w, "<html><body><h1>Success!</h1><h2>You can now close this tab.</h2></body></html>",
			)
		} else {
			io.WriteString(
				w, "<html><body><h1>Authorization failed.</h1><h2>Please try again</h2></body></html>",
			)
		}
	}

	http.HandleFunc("/", redirectHandler)

	serverErrorChannel := make(chan error)
	defer close(serverErrorChannel)

	go func() {
		err := server.ListenAndServe()
		if err != nil {
			serverErrorChannel <- err
		}
	}()
	defer server.Close()

	timer := time.NewTimer(100 * time.Millisecond)

	select {
	case <-timer.C:
		log.Info.Printf("Visit the following URL to authorize eztoggl:\n%s", authURL)
		openURL(authURL)
	case err := <-serverErrorChannel:
		<-timer.C
		return nil, fmt.Errorf("http server error: %s", err)
	}

	var authCode string
	select {
	case authCode = <-tokenChannel:
		server.Close()
		<-serverErrorChannel
	case err := <-serverErrorChannel:
		return nil, fmt.Errorf("http server error: %s", err)
	}

	token, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		return nil, err
	}

	log.Info.Println("Authorization successful!")

	return token, nil
}

func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	token := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(token)
	return token, err
}

func saveToken(path string, token *oauth2.Token) error {
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		return fmt.Errorf("could not cache oauth2 token: %s", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)

	log.Info.Printf("Token saved to %s", path)

	return nil
}
