package timeutil

import (
	"fmt"
	"time"
)

// MsToHours converts milliseconds hours.
func MsToHours(ms int) float64 {
	msFloat := float64(ms)
	seconds := msFloat / 1000
	minutes := seconds / 60
	hours := minutes / 60

	return hours
}

// TimeToDateStr formats the given `time` to a `"YYYY-MM-DD"` string
func TimeToDateStr(time time.Time) string {
	year, month, day := time.Date()
	dateString := fmt.Sprintf("%d-%02d-%02d", year, month, day)
	return dateString
}

// GetLastMonthsBoundaries returns the beginning and end of last month as `time.Time`.
func GetLastMonthsBoundaries() (time.Time, time.Time) {
	now := time.Now()

	currentYear, currentMonth, _ := now.Date()
	endOfLastMonth := time.Date(currentYear, currentMonth, 1, 0, 0, 0, 0, now.Location()).
		Add(-time.Nanosecond)

	targetYear, targetMonth, _ := endOfLastMonth.Date()
	beginningOfLastMonth := time.Date(targetYear, targetMonth, 1, 0, 0, 0, 0, now.Location())

	return beginningOfLastMonth, endOfLastMonth
}
