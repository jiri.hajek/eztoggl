package fakturoid

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"

	"gitlab.com/jiri.hajek/eztoggl/internal/env"
)

type bankAccount struct {
	ID   int
	Name string
}

type client struct {
	ID   int
	Name string
	Type string
}

// InvoiceLine represents a single line of a Fakturoid invoice.
type InvoiceLine struct {
	Name      string `json:"name"`
	Quantity  string `json:"quantity"`
	UnitName  string `json:"unit_name"`
	UnitPrice string `json:"unit_price"`
	VatRate   int    `json:"vat_rate"`
}

// Invoice represents a Fakturoid invoice.
type Invoice struct {
	ID     int    `json:"id,omitempty"`
	PDFURL string `json:"pdf_url,omitempty"`

	BankAccountID int           `json:"bank_account_id"`
	ClientID      int           `json:"subject_id"`
	Currency      string        `json:"currency"`
	DateIssued    string        `json:"issued_on"`
	DaysUntilDue  int           `json:"due"`
	Lines         []InvoiceLine `json:"lines"`
	PaymentMethod string        `json:"payment_method"`
}

// Fakturoid represents a Fakturoid service configuration.
type Fakturoid struct {
	APIToken     *oauth2.Token
	BaseURL      string
	ClientID     string
	ClientSecret string
	Email        string
	Slug         string
}

// New creates a new instance of Fakturoid service configuration.
func New(
	clientID string,
	clientSecret string,
	email string,
	slug string,
) *Fakturoid {
	fkt := Fakturoid{
		BaseURL:      "https://app.fakturoid.cz/api/v3",
		ClientID:     clientID,
		ClientSecret: clientSecret,
		Email:        email,
		Slug:         slug,
	}

	return &fkt
}

// NewInvoice is a helper function for creating `Invoice` objects.
func NewInvoice(
	bankAccountID int,
	clientID int,
	currency string,
	dateIssued string,
	daysUntilDue int,
	lines []InvoiceLine,
) *Invoice {
	invoice := &Invoice{
		BankAccountID: bankAccountID,
		ClientID:      clientID,
		Currency:      currency,
		DateIssued:    dateIssued,
		DaysUntilDue:  daysUntilDue,
		Lines:         lines,
		PaymentMethod: "bank",
	}

	return invoice
}

// CustomTransport adds custom headers to the token request
type CustomTransport struct {
	Base http.RoundTripper
}

func (t *CustomTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	// Add your custom header
	req.Header.Add("Accept", "application/json")
	return t.Base.RoundTrip(req)
}

func (fkt *Fakturoid) token() (*oauth2.Token, error) {
	// Create an HTTP client with the custom transport
	customClient := &http.Client{
		Transport: &CustomTransport{
			Base: http.DefaultTransport,
		},
	}

	config := &clientcredentials.Config{
		ClientID:     fkt.ClientID,
		ClientSecret: fkt.ClientSecret,
		TokenURL:     fmt.Sprintf("%s/oauth/token", fkt.BaseURL),
		AuthStyle:    oauth2.AuthStyleInHeader,
	}

	ctx := context.WithValue(context.Background(), oauth2.HTTPClient, customClient)
	token, err := config.Token(ctx)
	if err != nil {
		return nil, fmt.Errorf("Unable to retrieve token: %v", err)
	}

	return token, nil
}

func (fkt *Fakturoid) request(method string, url string, body io.Reader) (*http.Response, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	if !fkt.APIToken.Valid() {
		token, err := fkt.token()
		if err != nil {
			return nil, err
		}

		fkt.APIToken = token
	}

	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("User-Agent", env.UserAgent)
	req.Header.Add("Authorization", fmt.Sprintf("%s %s", fkt.APIToken.Type(), fkt.APIToken.AccessToken))

	client := http.DefaultClient
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	return res, nil
}

// GetBankAccountID returns id of the specified Fakturoid bank account.
func (fkt *Fakturoid) GetBankAccountID(bankAccountName string) (int, error) {
	url := fmt.Sprintf(
		"%s/accounts/%s/bank_accounts.json",
		fkt.BaseURL,
		fkt.Slug,
	)

	res, err := fkt.request("GET", url, nil)
	if err != nil {
		return 0, err
	}
	defer res.Body.Close()

	bytes, err := io.ReadAll(res.Body)
	if err != nil {
		return 0, fmt.Errorf("could not read api response: %s", err)
	}

	if res.StatusCode != 200 {
		return 0, fmt.Errorf("server responded with code %d, response: %s", res.StatusCode, string(bytes))
	}

	var bankAccounts []bankAccount
	err = json.Unmarshal(bytes, &bankAccounts)
	if err != nil {
		return 0, fmt.Errorf("could not parse api response: %s", err)
	}

	for _, bankAccount := range bankAccounts {
		if bankAccount.Name == bankAccountName {
			return bankAccount.ID, nil
		}
	}

	if len(bankAccounts) == 1 {
		return 0, fmt.Errorf(
			"could not find bank account %q, did you mean %q?", bankAccountName, bankAccounts[0].Name,
		)
	}

	return 0, fmt.Errorf("could not find bank account %q", bankAccountName)
}

// GetClientID returns id of the specified Fakturoid client.
func (fkt *Fakturoid) GetClientID(clientName string) (int, error) {
	url := fmt.Sprintf(
		"%s/accounts/%s/subjects/search.json?query=%s",
		fkt.BaseURL,
		url.QueryEscape(fkt.Slug),
		url.QueryEscape(clientName),
	)

	res, err := fkt.request("GET", url, nil)
	if err != nil {
		return 0, err
	}
	defer res.Body.Close()

	bytes, err := io.ReadAll(res.Body)
	if err != nil {
		return 0, fmt.Errorf("could not read api response: %s", err)
	}

	if res.StatusCode != 200 {
		return 0, fmt.Errorf("server responded with code %d, response: %s", res.StatusCode, string(bytes))
	}

	var clients []client
	err = json.Unmarshal(bytes, &clients)
	if err != nil {
		return 0, fmt.Errorf("could not parse api response: %s", err)
	}

	for _, client := range clients {
		if client.Name == clientName && client.Type == "customer" {
			return client.ID, nil
		}
	}

	if len(clients) == 1 {
		return 0, fmt.Errorf(
			"could not find client %q, did you mean %q?", clientName, clients[0].Name,
		)
	}

	return 0, fmt.Errorf("could not find client %q", clientName)
}

// GenerateInvoice creates a Fakturoid invoice and returns it as PDF.
func (fkt *Fakturoid) GenerateInvoice(invoice *Invoice) ([]byte, error) {
	url := fmt.Sprintf(
		"%s/accounts/%s/invoices.json",
		fkt.BaseURL,
		url.QueryEscape(fkt.Slug),
	)

	payload, err := json.Marshal(invoice)
	if err != nil {
		return nil, fmt.Errorf("could not encode payload: %s", err)
	}

	res, err := fkt.request("POST", url, bytes.NewBuffer(payload))
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	bytes, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("could not read api response: %s", err)
	}

	if res.StatusCode != 201 {
		return nil, fmt.Errorf("server responded with code %d, response: %s", res.StatusCode, string(bytes))
	}

	var response Invoice
	err = json.Unmarshal(bytes, &response)
	if err != nil {
		return nil, fmt.Errorf("could not parse api response: %s", err)
	}

	for i := 0; i < 30; i++ {
		res, err = fkt.request("GET", response.PDFURL, nil)
		if err != nil {
			return nil, err
		}
		defer res.Body.Close()

		bytes, err = io.ReadAll(res.Body)
		if err != nil {
			return nil, fmt.Errorf("could not read api response: %s", err)
		}

		switch res.StatusCode {
		case 200:
			return bytes, nil
		case 204:
			// waiting for Fakturoid to generate the PDF
		default:
			return nil, fmt.Errorf("server responded with code %d, response: %s", res.StatusCode, string(bytes))
		}

		time.Sleep(1 * time.Second)
	}

	return nil, fmt.Errorf("timed out")
}
