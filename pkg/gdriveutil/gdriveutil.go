package gdriveutil

import (
	"context"
	"fmt"
	"io"
	"os"

	"golang.org/x/oauth2/google"
	"google.golang.org/api/drive/v3"
	"google.golang.org/api/option"

	"gitlab.com/jiri.hajek/eztoggl/internal/oauth2util"
)

// GetService returns a Google Drive v3 API service based on given app credentials, OAuth2 token and scopes. If the token file does not exist, a browser tab with Google's authorization flow will be opened.
func GetService(pathToCredentialsJSON string, pathToTokenJSON string, scopes ...string) (*drive.Service, error) {
	credentials, err := os.ReadFile(pathToCredentialsJSON)
	if err != nil {
		return nil, fmt.Errorf("could not read credentials file %s", pathToCredentialsJSON)
	}

	oauth2Config, err := google.ConfigFromJSON(credentials, drive.DriveMetadataReadonlyScope, drive.DriveFileScope)
	if err != nil {
		return nil, fmt.Errorf("could not construct oauth2 config from credentials")
	}

	client, err := oauth2util.GetClient(pathToTokenJSON, oauth2Config)
	if err != nil {
		return nil, fmt.Errorf("could not get oauth2 client: %s", err)
	}

	service, err := drive.NewService(context.Background(), option.WithHTTPClient(client))
	if err != nil {
		return nil, fmt.Errorf("could not get gdrive service: %s", err)
	}

	return service, err
}

// GetFolderID attempts to find a Google Drive folder by name and retrieve its ID. If there are multiple folders with the same name, an error is returned.
func GetFolderID(service *drive.Service, folderName string) (string, error) {
	baseQueryCall := service.Files.List().SupportsAllDrives(true).IncludeItemsFromAllDrives(true)
	isFolderQuery := "mimeType = 'application/vnd.google-apps.folder'"

	query := fmt.Sprintf("name = '%s' and %s", folderName, isFolderQuery)
	queryResult, err := baseQueryCall.Q(query).Do()
	if err != nil {
		return "", err
	}

	var folderID string
	switch len(queryResult.Files) {
	case 0:
		return "", fmt.Errorf("folder %q does not exist", folderName)
	case 1:
		folderID = queryResult.Files[0].Id
	default:
		return "", fmt.Errorf("found multiple folders with name %q", folderName)
	}

	return folderID, nil
}

func createFile(service *drive.Service, file *drive.File, content io.Reader) (*drive.File, error) {
	createdFile, err := service.Files.Create(file).Media(content).SupportsAllDrives(true).Do()
	if err != nil {
		return nil, err
	}

	return createdFile, nil
}

// UploadFileToRoot uploads a file to the root Google Drive folder.
func UploadFileToRoot(service *drive.Service, name string, mimeType string, content io.Reader) (*drive.File, error) {
	newFile := &drive.File{
		MimeType: mimeType,
		Name:     name,
	}

	return createFile(service, newFile, content)
}

// UploadFileToFolder uploads a file to the specified Google Drive folder.
func UploadFileToFolder(service *drive.Service, folderID string, name string, mimeType string, content io.Reader) (*drive.File, error) {
	newFile := &drive.File{
		MimeType: mimeType,
		Name:     name,
		Parents:  []string{folderID},
	}

	return createFile(service, newFile, content)
}
