package toggl

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"

	"gitlab.com/jiri.hajek/eztoggl/internal/env"
	"gitlab.com/jiri.hajek/eztoggl/internal/timeutil"
)

type workspace struct {
	ID   int
	Name string
}

type client struct {
	ID   int
	Name string
}

type summaryResponse struct {
	Data []struct {
		Title struct {
			Project interface{}
		}
		Time int
	}
}

type summaryEntry struct {
	Project string
	Time    float64
}

// Summary represents Toggl summary report data with total tracked time per project.
type Summary struct {
	Entries []summaryEntry
}

// Toggl represents a Toggl service configuration.
type Toggl struct {
	APIToken string
}

// New creates a new instance of Toggl service configuration.
func New(apiToken string) *Toggl {
	tgl := Toggl{
		APIToken: apiToken,
	}

	return &tgl
}

func (tgl *Toggl) request(method string, url string, body io.Reader) ([]byte, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("User-Agent", env.UserAgent)
	req.SetBasicAuth(tgl.APIToken, "api_token")

	client := http.DefaultClient
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	bytes, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("could not read api response: %s", err)
	}

	if res.StatusCode != 200 {
		return nil, fmt.Errorf("server responded with code %d, response: %s", res.StatusCode, string(bytes))
	}

	return bytes, nil
}

// GetWorkspaceID returns id of the specified Toggl workspace.
func (tgl *Toggl) GetWorkspaceID(workspaceName string) (int, error) {
	url := "https://api.track.toggl.com/api/v9/workspaces"

	response, err := tgl.request("GET", url, nil)
	if err != nil {
		return 0, err
	}

	var workspaces []workspace
	err = json.Unmarshal(response, &workspaces)
	if err != nil {
		return 0, fmt.Errorf("could not parse api response: %s", err)
	}

	for _, workspace := range workspaces {
		if workspace.Name == workspaceName {
			return workspace.ID, nil
		}
	}

	if len(workspaces) == 1 {
		return 0, fmt.Errorf(
			"could not find workspace %q, did you mean %q?", workspaceName, workspaces[0].Name,
		)
	}

	return 0, fmt.Errorf("could not find workspace %q", workspaceName)
}

// GetClientID returns id of the specified Toggl client within the given. Only the given workspace is searched.
func (tgl *Toggl) GetClientID(workspaceID int, clientName string) (int, error) {
	url := fmt.Sprintf("https://api.track.toggl.com/api/v9/workspaces/%d/clients", workspaceID)

	response, err := tgl.request("GET", url, nil)
	if err != nil {
		return 0, err
	}

	var clients []client
	err = json.Unmarshal(response, &clients)
	if err != nil {
		return 0, fmt.Errorf("could not parse api response: %s", err)
	}

	for _, client := range clients {
		if client.Name == clientName {
			return client.ID, nil
		}
	}

	if len(clients) == 1 {
		return 0, fmt.Errorf(
			"could not find client %q, did you mean %q?", clientName, clients[0].Name,
		)
	}

	return 0, fmt.Errorf("could not find client %q", clientName)
}

// GetSummaryReportPDF returns a Toggl summary report PDF for days between the specified dates.
func (tgl *Toggl) GetSummaryReportPDF(workspaceID int, clientID int, from time.Time, to time.Time) ([]byte, error) {
	url := fmt.Sprintf(
		"https://api.track.toggl.com/reports/api/v3/workspace/%d/summary/time_entries.pdf",
		workspaceID,
	)

	payload, err := json.Marshal(map[string]interface{}{
		"start_date":      timeutil.TimeToDateStr(from),
		"end_date":        timeutil.TimeToDateStr(to),
		"client_ids":      []int{clientID},
		"order_by":        "title",
		"order_dir":       "asc",
		"grouping":        "projects",
		"sub_grouping":    "time_entries",
		"duration_format": "decimal",
		"date_format":     "DD/MM/YYYY",
		"hide_amounts":    true,
		"hide_rates":      true,
	})
	if err != nil {
		return nil, fmt.Errorf("could not encode request payload: %s", err)
	}

	response, err := tgl.request("POST", url, bytes.NewBuffer(payload))
	if err != nil {
		return nil, err
	}

	return response, nil
}

// GetDetailedReportPDF returns a Toggl detailed report PDF for days between the specified dates.
func (tgl *Toggl) GetDetailedReportPDF(workspaceID int, clientID int, from time.Time, to time.Time) ([]byte, error) {
	url := fmt.Sprintf(
		"https://api.track.toggl.com/reports/api/v3/workspace/%d/search/time_entries.pdf?user_agent=%s",
		workspaceID,
		url.QueryEscape(env.UserAgent),
	)

	payload, err := json.Marshal(map[string]interface{}{
		"client_ids":      []int{clientID},
		"duration_format": "decimal",
		"order_dir":       "asc",
		"start_date":      timeutil.TimeToDateStr(from),
		"end_date":        timeutil.TimeToDateStr(to),
		"date_format":     "DD/MM/YYYY",
	})
	if err != nil {
		return nil, fmt.Errorf("could not encode request payload: %s", err)
	}

	response, err := tgl.request("POST", url, bytes.NewBuffer(payload))
	if err != nil {
		return nil, err
	}

	return response, nil
}

// GetSummary returns total tracked time per project between the specified dates.
func (tgl *Toggl) GetSummary(workspaceID int, clientID int, from time.Time, to time.Time) (*Summary, error) {
	url := fmt.Sprintf(
		"https://api.track.toggl.com/reports/api/v2/summary?user_agent=%s&workspace_id=%d&client_ids=%d&since=%s&until=%s&grouping=projects",
		url.QueryEscape(env.UserAgent),
		workspaceID,
		clientID,
		url.QueryEscape(timeutil.TimeToDateStr(from)),
		url.QueryEscape(timeutil.TimeToDateStr(to)),
	)

	response, err := tgl.request("GET", url, nil)
	if err != nil {
		return nil, err
	}

	var summaryResponse summaryResponse
	err = json.Unmarshal(response, &summaryResponse)
	if err != nil {
		return nil, fmt.Errorf("could not parse api response: %s", err)
	}

	summary := &Summary{}

	for _, elem := range summaryResponse.Data {
		time := timeutil.MsToHours(elem.Time)
		project, ok := elem.Title.Project.(string)
		if !ok {
			return nil, fmt.Errorf(
				"found %.2f hours of work without a project",
				time,
			)
		}

		entry := summaryEntry{
			Project: project,
			Time:    time,
		}
		summary.Entries = append(summary.Entries, entry)
	}

	return summary, nil
}
